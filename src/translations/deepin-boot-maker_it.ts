<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>BMHandler</name>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="12"/>
        <source>Disk Format Error: Please reformat it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="15"/>
        <source>Insufficient disk Space: Ensure that the disk has enough free space</source>
        <translation>Spazio su disco insufficiente: assicurati di avere sufficiente spazio su disco</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="18"/>
        <source>Disk Mount Error: Insert the disk again or reboot to retry</source>
        <translation>Montaggio disco fallito: inserisci nuovamente il disco o riavvia</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="21"/>
        <source>Image Decompression Error: Verify md5 checksum of the image to ensure its integrity</source>
        <translation>Decompressione immagine fallita: verifica l&apos;md5 checksum dell&apos;immagine per garantirne l&apos;integrità</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="24"/>
        <source>Internal Error</source>
        <translation>Errore interno</translation>
    </message>
</context>
<context>
    <name>BMWindow</name>
    <message>
        <location filename="../app/bmwindow.cpp" line="79"/>
        <source>Boot Maker is a simple tool to write system image files into USB flash drives and other media.</source>
        <translation>Boot Maker è un semplice strumento per scrivere l&apos;immagine di Sistema su Pendrive USB o altri supporti.
Localizzazione italiana a cura di Massimo A. Carofano</translation>
    </message>
    <message>
        <location filename="../app/bmwindow.cpp" line="81"/>
        <source>Boot Maker</source>
        <translation>Boot Maker</translation>
    </message>
</context>
<context>
    <name>ISOSelectView</name>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="125"/>
        <source>Drag an ISO image file here</source>
        <translation>Trascina qui la ISO</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="144"/>
        <source>OR</source>
        <translation>O</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="89"/>
        <location filename="../app/view/isoselectview.cpp" line="163"/>
        <source>Select an ISO image file</source>
        <translation>Seleziona un&apos;immagine ISO</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="214"/>
        <source>Next</source>
        <translation>Avanti</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="300"/>
        <source>Reselect an ISO image file</source>
        <translation>Riseleziona un&apos;immagine ISO</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="237"/>
        <source>Illegal ISO image file</source>
        <translation>File immagine ISO non legale</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="441"/>
        <source>Detecting ISO file, please wait...</source>
        <translation>Analisi file ISO, attendere prego...</translation>
    </message>
</context>
<context>
    <name>ProgressView</name>
    <message>
        <location filename="../app/view/progressview.cpp" line="62"/>
        <source>Burning</source>
        <translation>Scrivi</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="78"/>
        <source>Burning, please wait...</source>
        <translation>Scrittura in corso attendere prego...</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="90"/>
        <source>Do not remove the disk or shut down the computer during the process</source>
        <translation>Non rimuovere il disco o spegnere il computer durante il processo</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="112"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../libdbm/util/deviceinfo.h" line="40"/>
        <source>Removable Disk</source>
        <translation>Disco rimobibile</translation>
    </message>
    <message>
        <location filename="../libdbm/util/utils.cpp" line="221"/>
        <location filename="../libdbm/util/utils.cpp" line="365"/>
        <source>Removable disk</source>
        <translation>Disco rimobibile</translation>
    </message>
    <message>
        <location filename="../app/main.cpp" line="118"/>
        <source>Boot Maker</source>
        <translation>Boot Maker</translation>
    </message>
</context>
<context>
    <name>ResultView</name>
    <message>
        <location filename="../app/view/resultview.cpp" line="149"/>
        <source>Reboot now</source>
        <translation>Riavvia ora</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="132"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="66"/>
        <source>Successful</source>
        <translation>Fatto</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="229"/>
        <source>The error log will be uploaded automatically with the feedback. We cannot improve without your feedback</source>
        <translation>Il log di errore sarà inviato automaticamente col feedback. Non possiamo migliorare senza il tuo contributo</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="230"/>
        <source>Submit Feedback</source>
        <translation>Invia feedback</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="253"/>
        <source>After-Sale Services</source>
        <translation>Servizi post-vendita</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="269"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="277"/>
        <source>Sorry, process failed</source>
        <translation>Ops, creazione fallita</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="278"/>
        <source>Process failed</source>
        <translation>Processo fallito</translation>
    </message>
</context>
<context>
    <name>UnmountUsbView</name>
    <message>
        <location filename="../app/view/unmountusbview.cpp" line="13"/>
        <source>Verifying data and safely removing the media, please wait...</source>
        <translation>Verifica dati e rimozione sicura del dispositivo, attendere prego...</translation>
    </message>
</context>
<context>
    <name>UsbSelectView</name>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="87"/>
        <source>Select a partition</source>
        <translation>Seleziona una partizione</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="114"/>
        <source>Format the partition</source>
        <translation>Formatta la partizione</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="162"/>
        <source>Start</source>
        <translation>Inizio</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="166"/>
        <source>Back</source>
        <translation>Indietro</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="222"/>
        <source>Formatting will erase all data on the partition, but can increase the success rate, please confirm before proceeding</source>
        <translation>Formattando eliminerai tutti i dati e le partizioni, incrementando però le possibilità di successo, desideri proseguire?</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="318"/>
        <source>Formatting the partition will overwrite all data, please have a backup before proceeding.</source>
        <translation>Formattando la partizione sovrascriverai tutti i dati, assicurati di avere un backup prima di procedere.</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="335"/>
        <source>Disk Format Error: Please format the partition with FAT32</source>
        <translation>Formato disco errato: per cortesia formatta la partizione in FAT32</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="147"/>
        <source>No disk available</source>
        <translation>Nessun disco disponibile</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="316"/>
        <source>Format Partition</source>
        <translation>Formatta partizione</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="319"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="320"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
</TS>
