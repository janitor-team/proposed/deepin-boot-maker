<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bn">
<context>
    <name>BMHandler</name>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="12"/>
        <source>Disk Format Error: Please reformat it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="15"/>
        <source>Insufficient disk Space: Ensure that the disk has enough free space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="18"/>
        <source>Disk Mount Error: Insert the disk again or reboot to retry</source>
        <translation>ডিস্ক মাউন্টে ত্রুটি: ডিস্কটি পুনরায় লাগান অথবা আবার চেষ্টা করতে সিস্টেমটি রিবুট করুন</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="21"/>
        <source>Image Decompression Error: Verify md5 checksum of the image to ensure its integrity</source>
        <translation>ইমেজ ডিকম্প্রেশনে ত্রুটি: ইমেজের md5 checksum পরীক্ষা করুন এবং নিশ্চিত করুন যে ইমেজটি সম্পূর্ণ </translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="24"/>
        <source>Internal Error</source>
        <translation>অভ্যন্তরীণ ত্রুটি</translation>
    </message>
</context>
<context>
    <name>BMWindow</name>
    <message>
        <location filename="../app/bmwindow.cpp" line="79"/>
        <source>Boot Maker is a simple tool to write system image files into USB flash drives and other media.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/bmwindow.cpp" line="81"/>
        <source>Boot Maker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ISOSelectView</name>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="144"/>
        <source>OR</source>
        <translation>অথবা</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="89"/>
        <location filename="../app/view/isoselectview.cpp" line="163"/>
        <source>Select an ISO image file</source>
        <translation>একটি ISO ফাইল নির্বাচন করুন</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="125"/>
        <source>Drag an ISO image file here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="214"/>
        <source>Next</source>
        <translation>পরবর্তী</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="237"/>
        <source>Illegal ISO image file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="300"/>
        <source>Reselect an ISO image file</source>
        <translation>পুনরায় একটি ISO ফাইল নির্বাচন করুন</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="441"/>
        <source>Detecting ISO file, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgressView</name>
    <message>
        <location filename="../app/view/progressview.cpp" line="62"/>
        <source>Burning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="78"/>
        <source>Burning, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="90"/>
        <source>Do not remove the disk or shut down the computer during the process</source>
        <translation>প্রসেসটি চলাকালে ডিস্কটি খুলে ফেলবেন না অথবা কম্পিউটারটি বন্ধ করবেন না</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="112"/>
        <source>Cancel</source>
        <translation>বাতিল</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../libdbm/util/deviceinfo.h" line="40"/>
        <source>Removable Disk</source>
        <translation>অপসারণযোগ্য ডিস্ক</translation>
    </message>
    <message>
        <location filename="../libdbm/util/utils.cpp" line="221"/>
        <location filename="../libdbm/util/utils.cpp" line="365"/>
        <source>Removable disk</source>
        <translation>অপসারণযোগ্য ডিস্ক</translation>
    </message>
    <message>
        <location filename="../app/main.cpp" line="118"/>
        <source>Boot Maker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResultView</name>
    <message>
        <location filename="../app/view/resultview.cpp" line="149"/>
        <source>Reboot now</source>
        <translation>এখনি পুনঃরায় চালু করুন</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="132"/>
        <source>Done</source>
        <translation>শেষ</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="66"/>
        <source>Successful</source>
        <translation>সফল হয়েছে</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="229"/>
        <source>The error log will be uploaded automatically with the feedback. We cannot improve without your feedback</source>
        <translation>এই ত্রুটির লগটি আপনা আপনিই প্রতিক্রিয়া সহকারে আপলোড হবে।  আমরা আপনাদের প্রতিক্রিয়া ছাড়া উন্নত করতে পারবনা</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="230"/>
        <source>Submit Feedback</source>
        <translation>প্রতিক্রিয়া জমা দিন</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="253"/>
        <source>After-Sale Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="269"/>
        <source>Close</source>
        <translation>বন্ধ</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="277"/>
        <source>Sorry, process failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="278"/>
        <source>Process failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnmountUsbView</name>
    <message>
        <location filename="../app/view/unmountusbview.cpp" line="13"/>
        <source>Verifying data and safely removing the media, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UsbSelectView</name>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="87"/>
        <source>Select a partition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="114"/>
        <source>Format the partition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="147"/>
        <source>No disk available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="162"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="166"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="222"/>
        <source>Formatting will erase all data on the partition, but can increase the success rate, please confirm before proceeding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="316"/>
        <source>Format Partition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="318"/>
        <source>Formatting the partition will overwrite all data, please have a backup before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="335"/>
        <source>Disk Format Error: Please format the partition with FAT32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="319"/>
        <source>Cancel</source>
        <translation>বাতিল করুন</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="320"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
