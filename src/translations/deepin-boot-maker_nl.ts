<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>BMHandler</name>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="12"/>
        <source>Disk Format Error: Please reformat it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="15"/>
        <source>Insufficient disk Space: Ensure that the disk has enough free space</source>
        <translation>Onvoldoende schijfruimte: zorg er voor dat de schijf beschikt over %1 vrije ruimte</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="18"/>
        <source>Disk Mount Error: Insert the disk again or reboot to retry</source>
        <translation>Schijfaankoppelfout: sluit de schijf opnieuw aan of herstart het systeem om opnieuw te proberen</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="21"/>
        <source>Image Decompression Error: Verify md5 checksum of the image to ensure its integrity</source>
        <translation>Schijfafbeeldingsuitpakfout: controleer de md5-controlesom van het imagebestand en zorg ervoor dat het bestand volledig is</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="24"/>
        <source>Internal Error</source>
        <translation>Interne fout</translation>
    </message>
</context>
<context>
    <name>BMWindow</name>
    <message>
        <location filename="../app/bmwindow.cpp" line="79"/>
        <source>Boot Maker is a simple tool to write system image files into USB flash drives and other media.</source>
        <translation>Deepin Opstartmediummaker is een eenvoudig hulpmiddel om installatiemedia van een systeem naar een cd/dvd, usb-station en andere installatiemedia te schrijven.</translation>
    </message>
    <message>
        <location filename="../app/bmwindow.cpp" line="81"/>
        <source>Boot Maker</source>
        <translation>Opstartmediummaker</translation>
    </message>
</context>
<context>
    <name>ISOSelectView</name>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="125"/>
        <source>Drag an ISO image file here</source>
        <translation>Sleep een ISO-imagebestand hierheen</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="144"/>
        <source>OR</source>
        <translation>OF</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="89"/>
        <location filename="../app/view/isoselectview.cpp" line="163"/>
        <source>Select an ISO image file</source>
        <translation>Selecteer een ISO-imagebestand</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="214"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="300"/>
        <source>Reselect an ISO image file</source>
        <translation>Herselecteer een ISO-imagebestand</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="237"/>
        <source>Illegal ISO image file</source>
        <translation>Beschadigd ISO-imagebestand</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="441"/>
        <source>Detecting ISO file, please wait...</source>
        <translation>Bezig met herkennen van ISO-bestand...</translation>
    </message>
</context>
<context>
    <name>ProgressView</name>
    <message>
        <location filename="../app/view/progressview.cpp" line="62"/>
        <source>Burning</source>
        <translation>Branden</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="78"/>
        <source>Burning, please wait...</source>
        <translation>Bezig met branden...</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="90"/>
        <source>Do not remove the disk or shut down the computer during the process</source>
        <translation>Koppel de schijf niet af of sluit de computer niet af tijdens de procedure</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="112"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../libdbm/util/deviceinfo.h" line="40"/>
        <source>Removable Disk</source>
        <translation>Verwijderbare schijf</translation>
    </message>
    <message>
        <location filename="../libdbm/util/utils.cpp" line="221"/>
        <location filename="../libdbm/util/utils.cpp" line="365"/>
        <source>Removable disk</source>
        <translation>Verwijderbare schijf</translation>
    </message>
    <message>
        <location filename="../app/main.cpp" line="118"/>
        <source>Boot Maker</source>
        <translation>Opstartmediummaker</translation>
    </message>
</context>
<context>
    <name>ResultView</name>
    <message>
        <location filename="../app/view/resultview.cpp" line="149"/>
        <source>Reboot now</source>
        <translation>Nu herstarten</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="132"/>
        <source>Done</source>
        <translation>Klaar</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="66"/>
        <source>Successful</source>
        <translation>Succesvol</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="229"/>
        <source>The error log will be uploaded automatically with the feedback. We cannot improve without your feedback</source>
        <translation>Het foutrapport zal automatisch worden geüpload. We hebben je feedback nodig om te blijven innoveren.</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="230"/>
        <source>Submit Feedback</source>
        <translation>Feedback versturen</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="253"/>
        <source>After-Sale Services</source>
        <translation>Naverkoopdiensten</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="269"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="277"/>
        <source>Sorry, process failed</source>
        <translation>Sorry, procedure mislukt</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="278"/>
        <source>Process failed</source>
        <translation>Procedure mislukt</translation>
    </message>
</context>
<context>
    <name>UnmountUsbView</name>
    <message>
        <location filename="../app/view/unmountusbview.cpp" line="13"/>
        <source>Verifying data and safely removing the media, please wait...</source>
        <translation>Bezig met verifiëren van gegevens en veilig uitwerpen...</translation>
    </message>
</context>
<context>
    <name>UsbSelectView</name>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="87"/>
        <source>Select a partition</source>
        <translation>Kies een partitie</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="114"/>
        <source>Format the partition</source>
        <translation>Partitie formatteren</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="162"/>
        <source>Start</source>
        <translation>Starten</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="166"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="222"/>
        <source>Formatting will erase all data on the partition, but can increase the success rate, please confirm before proceeding</source>
        <translation>De schijfgegevens worden volledig verwijderd tijdens het formatteren. Bevestig en ga door.</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="318"/>
        <source>Formatting the partition will overwrite all data, please have a backup before proceeding.</source>
        <translation>De schijfgegevens worden volledig verwijderd tijdens het formatteren. Maak een back-up voordat je verder gaat.</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="335"/>
        <source>Disk Format Error: Please format the partition with FAT32</source>
        <translation>Schijfindelingsfout: formatteer de schijf met fat32</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="147"/>
        <source>No disk available</source>
        <translation>Geen beschikbare schijf aangetroffen</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="316"/>
        <source>Format Partition</source>
        <translation>Partitie formatteren</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="319"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="320"/>
        <source>OK</source>
        <translation>Oké</translation>
    </message>
</context>
</TS>
