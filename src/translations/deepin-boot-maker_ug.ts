<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>BMHandler</name>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="12"/>
        <source>Disk Format Error: Please reformat it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="15"/>
        <source>Insufficient disk Space: Ensure that the disk has enough free space</source>
        <translation>دىسكا بوشلۇقى يېتىشمىدى: يېتەرلىك بوشلۇق بولۇشقا كاپالەتلىك قىلىڭ</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="18"/>
        <source>Disk Mount Error: Insert the disk again or reboot to retry</source>
        <translation>دىسكا يۈكلەش خاتا: قايتىدىن دىسكا كىرگۈزۈڭ ياكى سىستېمىنى قايتا قوزغىتىپ سىناپ بېقىڭ</translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="21"/>
        <source>Image Decompression Error: Verify md5 checksum of the image to ensure its integrity</source>
        <translation>تەسۋىرنى پىرېستىن يېشىش مەغلۇب بولدى: تەسۋىر ھۆججىتىنىڭ md5 سىنى تەكشۈرۈپ سۈرەت ھۆججىتىنىڭ مۇكەممەل ئىكەنلىكىنى جەزملەشتۈرۈڭ </translation>
    </message>
    <message>
        <location filename="../libdbm/backend/bmhandler.cpp" line="24"/>
        <source>Internal Error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
</context>
<context>
    <name>BMWindow</name>
    <message>
        <location filename="../app/bmwindow.cpp" line="79"/>
        <source>Boot Maker is a simple tool to write system image files into USB flash drives and other media.</source>
        <translation>قوزغىتىش دىسكىسى ياساش قورالى بولسا سىستېما تەسۋىر ھۆججىتىنى ئوپتىكىلىق دىسكا، بارماق دىسكا قاتارلىق قاچىلاش ۋاسىتىلىرىغا يازىدىغان قورالچاقتۇر. </translation>
    </message>
    <message>
        <location filename="../app/bmwindow.cpp" line="81"/>
        <source>Boot Maker</source>
        <translation>قوزغىتىش تاختىسى ياساش قورالى</translation>
    </message>
</context>
<context>
    <name>ISOSelectView</name>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="125"/>
        <source>Drag an ISO image file here</source>
        <translation>ئوپتىكىلىق دىسكا تەسۋىر ھۆججىتىنى بۇ يەرگە ئەكىرىڭ</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="144"/>
        <source>OR</source>
        <translation>ياكى</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="89"/>
        <location filename="../app/view/isoselectview.cpp" line="163"/>
        <source>Select an ISO image file</source>
        <translation>ئوپتىكىلىق دىسكا تەسۋىر ھۆججىتىنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="214"/>
        <source>Next</source>
        <translation>كېيىنكى </translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="300"/>
        <source>Reselect an ISO image file</source>
        <translation>ئوپتىكىلىق دىسكا تەسۋىر ھۆججىتىنى قايتىدىن تاللاڭ</translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="237"/>
        <source>Illegal ISO image file</source>
        <translation>ISO ھۆججىتى قانۇنسىز </translation>
    </message>
    <message>
        <location filename="../app/view/isoselectview.cpp" line="441"/>
        <source>Detecting ISO file, please wait...</source>
        <translation>ISO ھۆججىتىنى تەكشۈرۈۋاتىدۇ، سەل ساقلاڭ...</translation>
    </message>
</context>
<context>
    <name>ProgressView</name>
    <message>
        <location filename="../app/view/progressview.cpp" line="62"/>
        <source>Burning</source>
        <translation>ياسىلىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="78"/>
        <source>Burning, please wait...</source>
        <translation>ياسىلىۋاتىدۇ، سەل ساقلاڭ...</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="90"/>
        <source>Do not remove the disk or shut down the computer during the process</source>
        <translation>ياسىلىۋاتقاندا دىسكىدىن چىقىرىۋەتمەڭ ياكى ئېتىۋەتمەڭ</translation>
    </message>
    <message>
        <location filename="../app/view/progressview.cpp" line="112"/>
        <source>Cancel</source>
        <translation>بىكار قىلىش</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../libdbm/util/deviceinfo.h" line="40"/>
        <source>Removable Disk</source>
        <translation>يۆتكىلىشچان دىسكا</translation>
    </message>
    <message>
        <location filename="../libdbm/util/utils.cpp" line="221"/>
        <location filename="../libdbm/util/utils.cpp" line="365"/>
        <source>Removable disk</source>
        <translation>يۆتكىلىشچان دىسكا</translation>
    </message>
    <message>
        <location filename="../app/main.cpp" line="118"/>
        <source>Boot Maker</source>
        <translation>قوزغىتىش تاختىسى ياساش قورالى</translation>
    </message>
</context>
<context>
    <name>ResultView</name>
    <message>
        <location filename="../app/view/resultview.cpp" line="149"/>
        <source>Reboot now</source>
        <translation>ھازىرلا قايتا قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="132"/>
        <source>Done</source>
        <translation>تامام</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="66"/>
        <source>Successful</source>
        <translation>ياساش مۇۋەپپەقىيەتلىك بولدى</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="229"/>
        <source>The error log will be uploaded automatically with the feedback. We cannot improve without your feedback</source>
        <translation>مەسىلە ئىنكاس قىلغاندا خاتا كۈندىلىك خاتىرە ئاپتوماتىك يوللىنىدۇ، تەرەققىياتىمىز سىزنىڭ ئىنكاسىڭىز ۋە قوللىشىڭىزدىن ئايرىلالمايدۇ </translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="230"/>
        <source>Submit Feedback</source>
        <translation>ئىنكاس تاپشۇرۇش</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="253"/>
        <source>After-Sale Services</source>
        <translation>سېتىشتىن كېيىنكى مۇلازىمەت</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="269"/>
        <source>Close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="277"/>
        <source>Sorry, process failed</source>
        <translation>ياساش مەغلۇپ بولدى، كەچۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../app/view/resultview.cpp" line="278"/>
        <source>Process failed</source>
        <translation>ياساش مەغلۇپ بولدى</translation>
    </message>
</context>
<context>
    <name>UnmountUsbView</name>
    <message>
        <location filename="../app/view/unmountusbview.cpp" line="13"/>
        <source>Verifying data and safely removing the media, please wait...</source>
        <translation>ماس قەدەملىك ئۇچۇر ۋە بىخەتەرلىك ئۆچۈرۈلىۋاتىدۇ، سەل ساقلاڭ...</translation>
    </message>
</context>
<context>
    <name>UsbSelectView</name>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="87"/>
        <source>Select a partition</source>
        <translation>رايون تاللاڭ</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="114"/>
        <source>Format the partition</source>
        <translation>رايوننى فورماتلاش</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="162"/>
        <source>Start</source>
        <translation>ياساشنى باشلاش</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="166"/>
        <source>Back</source>
        <translation>قايتىش</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="222"/>
        <source>Formatting will erase all data on the partition, but can increase the success rate, please confirm before proceeding</source>
        <translation>فورماتلاش رايوندىكى بارلىق سانلىق مەلۇماتلارنى ئۆچۈرۈۋېتىدۇ، ئەمما مۇۋەپپەقىيەت قازىنىش نىسبىتىنى ئاشۇرالايدۇ، داۋاملاشتۇرۇشتىن بۇرۇن جەزملەشتۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="318"/>
        <source>Formatting the partition will overwrite all data, please have a backup before proceeding.</source>
        <translation>رايوننى فورماتلىسىڭىز بارلىق سانلىق مەلۇماتلارنى ئۆچۈرۈۋېتىدۇ، زاپاسلىۋالغاندىن كېيىن داۋملاشتۇرۇڭ</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="335"/>
        <source>Disk Format Error: Please format the partition with FAT32</source>
        <translation>دىىسكا فورماتى خاتا: FAT32 فورمات ئارقىلىق دىسكىنى قايتىدىن فورماتلاڭ</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="147"/>
        <source>No disk available</source>
        <translation>ئىشلەتكىلى بولىدىغان دىسكا تېپىلمىدى</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="316"/>
        <source>Format Partition</source>
        <translation>رايوننى فورماتلاش</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="319"/>
        <source>Cancel</source>
        <translation>بىكار قىلىش</translation>
    </message>
    <message>
        <location filename="../app/view/usbselectview.cpp" line="320"/>
        <source>OK</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
</TS>
